# a-CODE - REA - Remote Event Applications

[a-CODE > ] - REA - "Remote Event Applications" serie.


At the [a-CODE](https://www.youtube.com/channel/UCECO7gFTlPcOQKmLsFMDMSA) youtube channel you can find the video about this  serie.


[a-CODE > ](https://www.youtube.com/channel/UCECO7gFTlPcOQKmLsFMDMSA) - https://www.youtube.com/channel/UCECO7gFTlPcOQKmLsFMDMSA



This is part of the examples those are used in the "Remote Event Applications" series.

This will show you:
How to develop a "Remote Event Server" really, really, quickilly,
with Delphi community Edition and RESTDataware.

and also It will show you:
How to develop a "Remote Event Client" really, really, quickilly,
with Delphi community Edition and RESTDataware.

Here you can start to deal with multi-tier Applications and also with EndPoint 
to be consumed by Mobile Apps.


**Dependencies**

[RESTDATAWARE](https://www.restdw.com.br/)  -  https://www.restdw.com.br/
    https://svn.code.sf.net/p/rest-dataware-componentes/dataware/trunk


