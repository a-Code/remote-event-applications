unit aCodeREMobile.View.Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  uRESTDWServerEvents, uDWAbout, uRESTDWBase, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Layouts;

type
  TForm3 = class(TForm)
    Label1: TLabel;
    Button1: TButton;
    RESTClientPooler1: TRESTClientPooler;
    DWClientEvents1: TDWClientEvents;
    Panel1: TPanel;
    StyleBook1: TStyleBook;
    Label2: TLabel;
    Layout1: TLayout;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

uses
  uDWJSONObject;

{$R *.fmx}
{$R *.LgXhdpiPh.fmx ANDROID}

procedure TForm3.Button1Click(Sender: TObject);
var
  vErrorMessage : String;
  vDWParams : TDWParams;
begin
	vDWParams := nil; //to initialize it

	DWClientEvents1
		.CreateDWParams(     // pressing Shift + Ctrl + Space you can see the params that this method need
			'test'           // the first param is the EventName, write 'test' as a string
			,vDWParams);     // the second param is the variable that will receive the response
			                 // write vDWParams
//Next line

	DWClientEvents1
		.SendEvent(         // pressing Shift + Ctrl + Space you can see the params that this method need
			'test'  		// the first param is the Remote Event Name
			,vDWParams 		// the second param is the variable that will receive the response
			,vErrorMessage);// the third param is the variable that will receive the error message

	if vErrorMessage <> EmptyStr then         // if "vErrorMessage" is not empty, then
		ShowMessage(vErrorMessage)
	else
		Label1
			.Text := vDWParams
							  .ItemsString['result']
								  .AsString;
end;

end.
