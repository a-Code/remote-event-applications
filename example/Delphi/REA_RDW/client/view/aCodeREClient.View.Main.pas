unit aCodeREClient.View.Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, uRESTDWServerEvents,
  uDWAbout, uRESTDWBase, Vcl.ExtCtrls;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Panel2: TPanel;
    RESTClientPooler1: TRESTClientPooler;
    DWClientEvents1: TDWClientEvents;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  uDWJSONObject;

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var
  vErrorMessage : String;
  vDWParams : TDWParams;
begin

  vDWParams := nil;

  DWClientEvents1
    .CreateDWParams(
      'test'
      ,vDWParams);

  DWClientEvents1
    .SendEvent(
      'test'
      ,vDWParams
      ,vErrorMEssage);

  if vErrorMessage <> EmptyStr then
    ShowMessage(vErrorMessage)
  else
    Panel2
      .Caption := vDWParams
                    .ItemsString['result']
                      .AsString;

end;

end.
