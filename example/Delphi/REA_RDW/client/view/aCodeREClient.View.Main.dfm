object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 231
  ClientWidth = 505
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 505
    Height = 81
    Align = alTop
    Alignment = taLeftJustify
    Color = clWindowText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clHighlightText
    Font.Height = -27
    Font.Name = 'Calibri'
    Font.Style = []
    Padding.Left = 20
    Padding.Top = 20
    Padding.Right = 20
    Padding.Bottom = 20
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitLeft = 168
    ExplicitTop = 64
    ExplicitWidth = 450
    object Label1: TLabel
      Left = 21
      Top = 21
      Width = 463
      Height = 39
      Align = alClient
      Caption = 'a-Code       "Remote Event Client"'
      ExplicitWidth = 360
      ExplicitHeight = 33
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 81
    Width = 505
    Height = 150
    Align = alClient
    Caption = 'Waiting...'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    ExplicitTop = 87
    object Button1: TButton
      Left = 21
      Top = 40
      Width = 75
      Height = 81
      Caption = 'Call the Remote Event'
      TabOrder = 0
      WordWrap = True
      OnClick = Button1Click
    end
  end
  object RESTClientPooler1: TRESTClientPooler
    DataCompression = True
    UrlPath = '/'
    Encoding = esUtf8
    hEncodeStrings = True
    Host = 'localhost'
    UserName = 'testserver'
    Password = 'testserver'
    ProxyOptions.BasicAuthentication = False
    ProxyOptions.ProxyPort = 0
    RequestTimeOut = 10000
    ThreadRequest = False
    AllowCookies = False
    HandleRedirects = False
    FailOver = False
    FailOverConnections = <>
    FailOverReplaceDefaults = False
    BinaryRequest = False
    CriptOptions.Use = False
    CriptOptions.Key = 'RDWBASEKEY256'
    UserAgent = 
      'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, l' +
      'ike Gecko) Chrome/41.0.2227.0 Safari/537.36'
    TokenOptions.Active = False
    TokenOptions.TokenHash = 'RDWTS_HASH'
    Left = 176
    Top = 105
  end
  object DWClientEvents1: TDWClientEvents
    ServerEventName = 'TMainDataModule.MainServerEvents'
    CriptOptions.Use = False
    CriptOptions.Key = 'RDWBASEKEY256'
    RESTClientPooler = RESTClientPooler1
    Events = <
      item
        Routes = [crAll]
        DWParams = <
          item
            TypeObject = toParam
            ObjectDirection = odOUT
            ObjectValue = ovString
            ParamName = 'result'
            Encoded = True
          end>
        JsonMode = jmDataware
        Name = 'test'
      end>
    Left = 280
    Top = 105
  end
end
