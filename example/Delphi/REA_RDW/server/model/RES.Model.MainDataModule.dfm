object MainDataModule: TMainDataModule
  OldCreateOrder = False
  Encoding = esASCII
  Height = 208
  Width = 329
  object MainServerEvents: TDWServerEvents
    IgnoreInvalidParams = False
    Events = <
      item
        Routes = [crGet, crPost]
        DWParams = <
          item
            TypeObject = toParam
            ObjectDirection = odOUT
            ObjectValue = ovString
            ParamName = 'result'
            Encoded = False
          end>
        JsonMode = jmDataware
        Name = 'test'
        OnReplyEvent = MainServerEventsEventstestReplyEvent
      end>
    Left = 64
    Top = 56
  end
end
