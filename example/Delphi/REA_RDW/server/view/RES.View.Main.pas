unit RES.View.Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, uDWAbout, uRESTDWBase,
  Vcl.ExtCtrls;

type
  TMainForm = class(TForm)
    MainServicePooler: TRESTServicePooler;
    Panel1: TPanel;
    Label1: TLabel;
    Panel2: TPanel;
    SwitchButton: TButton;
    procedure SwitchButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

uses
  RES.Model.MainDataModule;

{$R *.dfm}

procedure TMainForm.SwitchButtonClick(Sender: TObject);
begin

  MainServicePooler.ServerMethodClass := TMainDataModule;

  MainServicePooler.Active := not MainServicePooler.Active;

  if MainServicePooler.Active then
    SwitchButton.Caption := 'Stop'
  else
    SwitchButton.Caption := 'Start';

end;

end.
