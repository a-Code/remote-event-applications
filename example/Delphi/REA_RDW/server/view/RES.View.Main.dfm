object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'a-Code  -  Remote Event Server'
  ClientHeight = 209
  ClientWidth = 450
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 450
    Height = 81
    Align = alTop
    Alignment = taLeftJustify
    Color = clWindowText
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clHighlightText
    Font.Height = -27
    Font.Name = 'Calibri'
    Font.Style = []
    Padding.Left = 20
    Padding.Top = 20
    Padding.Right = 20
    Padding.Bottom = 20
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Label1: TLabel
      Left = 21
      Top = 21
      Width = 408
      Height = 39
      Align = alClient
      Caption = 'a-Code       "Remote Event Server"'
      ExplicitWidth = 366
      ExplicitHeight = 33
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 81
    Width = 450
    Height = 128
    Align = alClient
    BevelOuter = bvNone
    Color = clWhite
    Padding.Left = 10
    Padding.Top = 30
    Padding.Right = 30
    Padding.Bottom = 30
    ParentBackground = False
    TabOrder = 1
    ExplicitTop = 87
    object SwitchButton: TButton
      Left = 10
      Top = 30
      Width = 97
      Height = 68
      Align = alLeft
      Caption = 'Start'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = SwitchButtonClick
    end
  end
  object MainServicePooler: TRESTServicePooler
    Active = False
    CORS = False
    CORS_CustomHeaders.Strings = (
      'Access-Control-Allow-Origin=*'
      
        'Access-Control-Allow-Methods=GET, POST, PATCH, PUT, DELETE, OPTI' +
        'ONS'
      
        'Access-Control-Allow-Headers=Content-Type, Origin, Accept, Autho' +
        'rization, X-CUSTOM-HEADER')
    RequestTimeout = -1
    ServicePort = 8082
    ProxyOptions.Port = 8888
    TokenOptions.Active = False
    TokenOptions.ServerRequest = 'RESTDWServer01'
    TokenOptions.TokenHash = 'RDWTS_HASH'
    TokenOptions.LifeCycle = 30
    ServerParams.HasAuthentication = False
    ServerParams.UserName = 'testserver'
    ServerParams.Password = 'testserver'
    SSLMethod = sslvSSLv2
    SSLVersions = []
    Encoding = esUtf8
    ServerContext = 'restdataware'
    RootPath = '/'
    SSLVerifyMode = []
    SSLVerifyDepth = 0
    ForceWelcomeAccess = False
    CriptOptions.Use = False
    CriptOptions.Key = 'RDWBASEKEY256'
    MultiCORE = False
    Left = 224
    Top = 120
  end
end
