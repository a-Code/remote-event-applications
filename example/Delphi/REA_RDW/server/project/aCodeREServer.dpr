program aCodeREServer;

uses
  Vcl.Forms,
  RES.View.Main in '..\view\RES.View.Main.pas' {MainForm},
  RES.Model.MainDataModule in '..\model\RES.Model.MainDataModule.pas' {MainDataModule: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TMainDataModule, MainDataModule);
  Application.Run;
end.
